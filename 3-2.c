
//Antonio Silva Paucar
//CIS 314
//asssigment 3

#include <stdio.h>
#include <stdlib.h>
/*
Explanation    // source   cs.brown.edu/courses/cs033/docs/guides/x64_cheatsheet.pdf
assembly code
%rdi     a
%rsi     b
%rdx    c               parametros or arguments.


 %rax z

subq %rdx, %rsi     substraction     b=b-c
imulq %rsi, %rdi      signed multiplication   a=a*b
salq $63, %rsi         shift  left          b=b<<63;
sarq $63, %rsi         shift right         b=b>>63
movq %rdi, %rax     copy               z=a
xorq %rsi, %rax       powe               z=z^b


return z
 */


 int decode(long a, long b, long c){

    int z;

    b = b - c;
    a = a * b;
    b = b << 63;
    b = b >> 63;
    z = a;
    z = z ^ b;
    return z;
 }



int main(void)
{

   printf("decode(1, 2, 4) = %d\n", decode(1,2,4) );
   printf("decode(3, 5, 7) = %d\n", decode(3,5,7) );
   printf("decode(10,20, 30) = %d\n", decode(10,20,30) );

    return 0;

}
