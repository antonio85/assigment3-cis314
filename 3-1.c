//Antonio Silva Paucar
//CIS 314
//assigment 3

#include <stdio.h>
#include <stdlib.h>


struct IntArray{ //struct
	int length;
    	int* dataPtr;

};

int test(){ // source  www.stackoverflow.com/questions/17292545/how-to-check-if-the-input-is-a-number-or-not-in-c  (with some modifications so it can serve two other fuctions.

    char *p;
    char s[10000];
    int n;
    int digit;


    while (fgets(s, sizeof(s), stdin)) {
        n = strtol(s, &p, 10);
        if (p == s || *p != '\n') {
            printf("Invalid input. ");
        } else break;
    }

    return n;
}

struct IntArray* mallocintArray(int length){//return a struct pointer

    struct IntArray *ptr = malloc(sizeof(struct IntArray));// start the pointer to a struct
    ptr->length = length; // set the length
    ptr->dataPtr = malloc(sizeof(int)*(ptr->length));//start array inside the struct
    return ptr;
}

void freeIntArray(struct IntArray *arrayPtr){ // release memory
    free(arrayPtr->dataPtr);// release pointer of the data inside the struct first
    free(arrayPtr);// release pointer of the struct

}

void readIntArray(struct IntArray *array){

    int number;

     for(int i = 0; i< array->length; i++){
          printf("Enter input: ");
          number = test();// use the test function to check if the input is correct.
          array->dataPtr[i]=number;//accesing the struct array by pointer
     }
}




void swap(int *xp, int *yp){

    int a = *xp; // swapping by pointers so they change also outside this function. Passing by reference.
    *xp = *yp;
    *yp = a;

}

void sortIntArray (struct IntArray *array){

     /*  source: www.youtube.com/watch?v=xWBP4lzkoyM  How selection sort works.

        0 1 2 3 4 <- Index     my understanding
        ---------              ----------------
        2 5 1 3 4
	check index 0 and compare with each one of the other value until find the minimum
        2 5|1|3 4
        swap the for index 0 and the minimum from the rest of the array(dont check the previous)
        1 5 2 3 4
        now, check index 1 and look other minimum from the rest of the array
        1 5|2|3 4
        ...swap
        1 2 5 3 4
        check index 2 and find other minimum from array place higher than 2
        1 2 5|3|4
        swap
        1 2 3 5 4
        check index 3 and again,look for the min
        1 2 3 5|4|
        and swap
        1 2 3 4 5....end of the array and done.


     */
      int check_min;

      //printf("checking Sort");

      //example 2 3 1 4 5
      for(int i = 0; i < (array->length)-1; i++)
      {
	   check_min = i;//first index to check (temporal min)

           //c 1 2 3 4  index c = check  with each of the other numbers to find a less value
	   //2 3 1 4 5  values
	   //|1|2 3 4 5 4

	   for(int j = i+1; j < (array->length); j++)
	   {
               //printf(" valores i y i+1: %d %d \n",array->dataPtr[check_min],(array->dataPtr[j]));//debug
               if( (array->dataPtr[check_min]) > (array->dataPtr[j]) )// if it is less, it marks its position
		{
		  //printf("min %d\n",check_min);  debug
		  check_min = j;	//if there is a new min, change it and loop again.
		}

           }

	   //printf("%d,%d\n",i,check_min); debug
	   //printArray(array);

           swap(&array->dataPtr[i],&array->dataPtr[check_min]);//swap position
	   //printArray(array);

      }
      //printf("checking Sort salida");

}

void printArray(struct IntArray *array){

     printf("\n%c%c",'[',' ');
     for(int i = 0; i<(array->length); i++)
	{
     if(i!=(array->length)-1)
         printf("%d, ",array->dataPtr[i]);//goes through the array and print it.
     else
         printf("%d ",array->dataPtr[i]);

	}
     printf("%c\n",']');

}


int main(void)
{
    printf("Enter length: ");
    int length = test(); // get length

    struct IntArray* ptr =   mallocintArray(length);  // creates the struct and the length starts the array inside the struct

	readIntArray(ptr); // get inputs
	sortIntArray(ptr);  // sort array by selection sort
	printArray(ptr);    // print array
    freeIntArray(ptr); // free allocating memory.

	return 0;

}
